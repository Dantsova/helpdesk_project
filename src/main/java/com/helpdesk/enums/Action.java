package com.helpdesk.enums;

public enum Action {
    TICKET_IS_CREATED,
    TICKET_IS_EDITED,
    SUBMIT,
    CANCEL,
    APPROVE,
    DECLINE,
    ASSIGN_TO_ME,
    DONE,
}
