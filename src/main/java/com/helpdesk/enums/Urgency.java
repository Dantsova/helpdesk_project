package com.helpdesk.enums;

public enum Urgency {
    CRITICAL,
    HIGH,
    AVERAGE,
    LOW;
}
