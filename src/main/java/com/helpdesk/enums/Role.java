package com.helpdesk.enums;

public enum Role {
    EMPLOYEE,
    MANAGER,
    ENGINEER
}
