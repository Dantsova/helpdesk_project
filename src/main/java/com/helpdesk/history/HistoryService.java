package com.helpdesk.history;

import com.helpdesk.common.AbstractService;
import com.helpdesk.ticket.Ticket;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class HistoryService extends AbstractService<History, HistoryRepository> {

    @Autowired
    public HistoryService(HistoryRepository repository) {
        super(repository);
    }

    public Page<History> getHistoryByTicket(Ticket ticket, Pageable paging) {
        return repository.findByTicketId(ticket, paging);
    }

    public Page<History> get(Ticket ticket, Pageable paging) {
        return repository.findByTicketId(ticket, paging);
    }
}

