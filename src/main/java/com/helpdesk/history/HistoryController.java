package com.helpdesk.history;

import com.helpdesk.exception.ResourceNotFoundException;
import com.helpdesk.ticket.Ticket;
import com.helpdesk.ticket.TicketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/histories")
@RestController
public class HistoryController {
    private final HistoryService historyService;
    private final HistoryMapper historyMapper;

    @Autowired
    TicketService ticketService;

    @Autowired
    public HistoryController(HistoryService historyService, HistoryMapper historyMapper) {
        this.historyService = historyService;
        this.historyMapper = historyMapper;
    }

    @GetMapping("/{ticketId}")
    public ResponseEntity<Page<HistoryDto>> getByTicket(Pageable paging,
                                                        @RequestParam(name = "ticketId", required = false) Integer ticketId) throws ResourceNotFoundException {
        Ticket ticket = ticketService.get(ticketId);
        Page<History> historyPage = historyService.get(ticket, paging);
        return ResponseEntity
                .ok(historyPage.map(historyMapper::toDto));
    }
}