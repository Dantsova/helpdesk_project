package com.helpdesk.history;

import com.helpdesk.common.CommonMapper;
import com.helpdesk.exception.ResourceNotFoundException;
import com.helpdesk.ticket.Ticket;
import com.helpdesk.ticket.TicketService;
import com.helpdesk.user.User;
import com.helpdesk.user.UserService;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.*;
import org.springframework.beans.factory.annotation.Autowired;

@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR)
public abstract class HistoryMapper implements CommonMapper<History, HistoryDto> {
    @Autowired
    private UserService userService;

    @Autowired
    private TicketService ticketService;

    @Mapping(target = "ticketId", source = "ticketId.id")
    @Mapping(target = "userId", source = "userId.id")
    public abstract HistoryDto toDto(History historyEntity);

    @Mapping(target = "ticketId", source = "ticketId",qualifiedByName = "mapTicket")
    @Mapping(target = "userId", source = "userId", qualifiedByName = "mapUser")
    public abstract History toEntity(HistoryDto historyDto);

    @Named("mapUser")
    protected User mapUser(Integer userId) throws ResourceNotFoundException {
        return userService.get(userId);
    }

    @Named("mapTicket")
    protected Ticket mapCategory(Integer categoryId) throws ResourceNotFoundException {
        return ticketService.get(categoryId);
    }
}