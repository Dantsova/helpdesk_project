package com.helpdesk.history;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.helpdesk.common.AbstractDto;
import com.helpdesk.enums.Action;

import java.time.LocalDate;

public class HistoryDto extends AbstractDto {
    private Integer ticketId;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private LocalDate date;

    private Action action;
    private Integer userId;
    private String description;

    public HistoryDto() {
    }

    public HistoryDto(Integer ticketId, LocalDate date, Action action, Integer userId, String description) {
        this.ticketId = ticketId;
        this.date = date;
        this.action = action;
        this.userId = userId;
        this.description = description;
    }

    public Integer getTicketId() {
        return ticketId;
    }

    public void setTicketId(Integer ticketId) {
        this.ticketId = ticketId;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public Action getAction() {
        return action;
    }

    public void setAction(Action action) {
        this.action = action;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}