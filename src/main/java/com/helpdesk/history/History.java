package com.helpdesk.history;

import com.helpdesk.common.AbstractEntity;
import com.helpdesk.enums.Action;
import com.helpdesk.ticket.Ticket;
import com.helpdesk.user.User;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "HISTORY", schema = "ticketdb")
public class History extends AbstractEntity {
    private Ticket ticketId;
    private LocalDate date;
    private Action action;
    private User userId;
    private String description;

    public History() {
    }

    public History(Ticket ticketId, LocalDate date, Action action, User userId, String description) {
        this.ticketId = ticketId;
        this.date = date;
        this.action = action;
        this.userId = userId;
        this.description = description;
    }

    @ManyToOne
    @JoinColumn(name = "ticket_id", referencedColumnName = "id")
    public Ticket getTicketId() {
        return ticketId;
    }

    public void setTicketId(Ticket ticketId) {
        this.ticketId = ticketId;
    }

    @Column(name = "date", nullable = true)
    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    @Enumerated(EnumType.STRING)
    @Column(name = "action", nullable = true, length = 30)
    public Action getAction() {
        return action;
    }

    public void setAction(Action action) {
        this.action = action;
    }

    @ManyToOne
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    public User getUserId() {
        return userId;
    }

    public void setUserId(User userId) {
        this.userId = userId;
    }

    @Column(name = "description", nullable = true, length = 30)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        History that = (History) o;

        if (ticketId != null ? !ticketId.equals(that.ticketId) : that.ticketId != null) return false;
        if (date != null ? !date.equals(that.date) : that.date != null) return false;
        if (action != null ? !action.equals(that.action) : that.action != null) return false;
        if (userId != null ? !userId.equals(that.userId) : that.userId != null) return false;
        if (description != null ? !description.equals(that.description) : that.description != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = 1;
        result = 31 * result + (ticketId != null ? ticketId.hashCode() : 0);
        result = 31 * result + (date != null ? date.hashCode() : 0);
        result = 31 * result + (action != null ? action.hashCode() : 0);
        result = 31 * result + (userId != null ? userId.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        return result;
    }
}