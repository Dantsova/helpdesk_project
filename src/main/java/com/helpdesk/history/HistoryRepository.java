package com.helpdesk.history;

import com.helpdesk.common.CommonRepository;
import com.helpdesk.ticket.Ticket;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface HistoryRepository extends CommonRepository<History>,
        PagingAndSortingRepository<History, Integer> {

    Page<History> findByTicketId(Ticket ticketId, Pageable paging);
}
