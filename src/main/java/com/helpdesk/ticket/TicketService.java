package com.helpdesk.ticket;

import com.helpdesk.common.AbstractService;
import com.helpdesk.enums.*;
import com.helpdesk.exception.HelpDeskException;
import com.helpdesk.exception.ResourceNotFoundException;
import com.helpdesk.history.History;
import com.helpdesk.history.HistoryService;
import com.helpdesk.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static java.util.Objects.nonNull;

@Service
public class TicketService extends AbstractService<Ticket, TicketRepository> {
    private final HistoryService historyService;

    private List<Role> roleList = Arrays.asList(Role.EMPLOYEE, Role.MANAGER);

    @Autowired
    public TicketService(TicketRepository repository, HistoryService historyService) {
        super(repository);
        this.historyService = historyService;
    }

    public Ticket createWithHistoryMark(User authUser, Ticket rowTicket) throws HelpDeskException, ResourceNotFoundException {
        Ticket newTicket = repository.save(rowTicket);
        History newHistory = new History(newTicket, LocalDate.now(), Action.TICKET_IS_CREATED, authUser, "Ticket is created");
        History History = historyService.create(authUser, newHistory);
        return newTicket;
    }

    public Page<Ticket> generateTickets(User owner) {
        Page<Ticket> ticketPage = null;
        for (int i = 1; i < 5; i++) {
            Ticket newTicket =
                    new Ticket(
                            "ticket" + i,
                            "desk",
                            LocalDate.now(),
                            LocalDate.now(),
                            owner,
                            owner,
                            State.NEW,
                            repository.findByCategoryId(2).get(),
                            Urgency.CRITICAL,
                            owner);
            ticketPage.getContent().add(newTicket);
        }
        Iterable<Ticket> persistedTicket = repository.saveAll(ticketPage);
        List<Ticket> persistedTicketList = StreamSupport
                .stream(persistedTicket.spliterator(), false)
                .collect(Collectors.toList());
        return new PageImpl<>(persistedTicketList);
    }

    public Page<Ticket> getByOwner(User owner, Pageable paging) {
        return repository.findAllByOwnerId(owner, paging);
    }

    public Page<Ticket> getByRole(User owner, Pageable paging) {
        Page<Ticket> ticketPage = null;
        List<State> stateList;
        switch (owner.getRole()) {
            case EMPLOYEE:
                ticketPage = repository.findAllByOwnerId(owner,paging);
                break;
            case MANAGER:
                ticketPage = repository.findAllByOwnerId(owner,paging);
                ticketPage.getContent().addAll(repository.findByRoleAndState(Role.EMPLOYEE, State.NEW));

                stateList = Arrays.asList(State.APPROVED, State.DECLINED, State.CANCELED, State.IN_PROGRESS, State.DONE);
                for (State state : stateList) {
                    ticketPage.getContent().addAll(repository.findByApproverAndState(owner.getId(), state));
                }
                break;
            case ENGINEER:
                for (Role role : roleList) {
                    ticketPage.getContent().addAll(repository.findByRoleAndState(role, State.APPROVED));
                }
                ticketPage.getContent().addAll(repository.findAllByAssigneeIdAndState(owner, State.IN_PROGRESS));
                ticketPage.getContent().addAll(repository.findAllByAssigneeIdAndState(owner, State.DONE));
                break;
        }
        return ticketPage;
    }

    public Ticket getByIdAndOwnerId(Integer ownerId, Integer ticketId) throws ResourceNotFoundException {
        return repository.findByIdAndOwnerId(ownerId, ticketId)
                .orElseThrow(() -> new ResourceNotFoundException("Ticket"));
    }

    public Page<Ticket> getByState(State state, Pageable paging) {
        return repository
                .findAllByState(state, paging);
    }

    public Page<Ticket> getBySort(User ownerId, Pageable paging) {
        Page<Ticket> pagedResult = repository.findAllByOwnerId(ownerId, paging);
        return pagedResult;
    }

    public Page<Ticket> getByFilter(User authUser, Integer id, String name, Date desiredResolutionDate, Urgency urgency, State state, Pageable paging) {
        Page ticketPage = null;
        if (nonNull(id)) {
            ticketPage = repository.findAllByIdAndOwnerId(id, authUser, paging);
        }
        if (nonNull(name)) {
            ticketPage = repository.findAllByNameAndOwnerId(name, authUser, paging);
        }
        if (nonNull(desiredResolutionDate)) {
            ticketPage = repository.findAllByDesiredResolutionDateAndOwnerId(desiredResolutionDate, authUser, paging);
        }
        if (nonNull(urgency)) {
            ticketPage = repository.findAllByUrgencyIdAndOwnerId(urgency, authUser, paging);
        }
        if (nonNull(state)) {
            ticketPage = repository.findAllByStateAndOwnerId(state, authUser, paging);
        }
        return ticketPage;
    }

    public Page<Ticket> getFilteredTicketByRole(User authUser, Pageable paging) {
        Page<Ticket> ticketPage = null;
        switch (authUser.getRole()) {
            case EMPLOYEE:
                ticketPage = repository.findAllByOwnerId(authUser, paging);
                break;
            case MANAGER:
                ticketPage = repository.findAllByOwnerIdAndApproverId(authUser, authUser, paging);
                break;
            case ENGINEER:
                ticketPage = repository.findAllByAssigneeId(authUser, paging);
                break;
        }
        return ticketPage;
    }

    public void addHistoryOnUpdate(User authUser, Integer ticketId, Ticket rowTicket) throws HelpDeskException, ResourceNotFoundException {
        History newHistory = new History(rowTicket, LocalDate.now(), Action.TICKET_IS_EDITED, authUser, "Ticket is edited");
        History history = historyService.create(authUser, newHistory);
    }

    public Ticket transmitState(User authUser, Integer ticketId, Action action) throws HelpDeskException, ResourceNotFoundException {
        Ticket Ticket = get(ticketId);
        State currentState = Ticket.getState();
        switch (authUser.getRole()) {
            case EMPLOYEE:
                if (action.equals(Action.SUBMIT) && currentState.equals(State.DRAFT)) {
                    Ticket.setState(State.NEW);
                    return repository.save(Ticket);
                }
                if (action.equals(Action.CANCEL) && currentState.equals(State.DRAFT)) {
                    Ticket.setState(State.CANCELED);
                    return repository.save(Ticket);
                }
                if (action.equals(Action.SUBMIT) && currentState.equals(State.DECLINED)) {
                    Ticket.setState(State.NEW);
                    return repository.save(Ticket);
                }
                if (action.equals(Action.CANCEL) && currentState.equals(State.DECLINED)) {
                    Ticket.setState(State.CANCELED);
                    return repository.save(Ticket);
                }
                throw new HelpDeskException("Ticket n/a action denied for " + authUser.getRole());
            case MANAGER:
                if (action.equals(Action.SUBMIT) && (currentState.equals(State.DRAFT) || currentState.equals(State.DECLINED)) && authUser.equals(Ticket.getOwnerId())) {
                    Ticket.setState(State.NEW);
                    return repository.save(Ticket);
                }
                if (action.equals(Action.CANCEL) && (currentState.equals(State.DRAFT) || currentState.equals(State.DECLINED)) && authUser.equals(Ticket.getOwnerId())) {
                    Ticket.setState(State.CANCELED);
                    return repository.save(Ticket);
                }
                if (action.equals(Action.APPROVE) && currentState.equals(State.NEW) && Ticket.getOwnerId().getRole().equals(Role.EMPLOYEE)) {
                    Ticket.setState(State.APPROVED);
                    return repository.save(Ticket);
                }
                if (action.equals(Action.DECLINE) && currentState.equals(State.NEW) && Ticket.getOwnerId().getRole().equals(Role.EMPLOYEE)) {
                    Ticket.setState(State.DECLINED);
                    return repository.save(Ticket);
                }
                if (action.equals(Action.CANCEL) && currentState.equals(State.NEW) && Ticket.getOwnerId().getRole().equals(Role.EMPLOYEE)) {
                    Ticket.setState(State.CANCELED);
                    return repository.save(Ticket);
                }
                throw new HelpDeskException("Ticket n/a action denied for " + authUser.getRole());
            case ENGINEER:
                if (action.equals(Action.ASSIGN_TO_ME) && currentState.equals(State.APPROVED)) {
                    Ticket.setState(State.IN_PROGRESS);
                    return repository.save(Ticket);
                }
                if (action.equals(Action.CANCEL) && currentState.equals(State.APPROVED)) {
                    Ticket.setState(State.CANCELED);
                    return repository.save(Ticket);
                }
                if (action.equals(Action.DONE) && currentState.equals(State.IN_PROGRESS)) {
                    Ticket.setState(State.DONE);
                    return repository.save(Ticket);
                }
                throw new HelpDeskException("Ticket n/a action denied for " + authUser.getRole());
        }
        return Ticket;
    }

    public Page<Ticket> getByStateAndOwner(State state, User owner, Pageable paging) {
        return repository.findAllByStateAndOwnerId(state,owner,paging);
    }
}