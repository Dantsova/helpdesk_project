package com.helpdesk.ticket;

import com.helpdesk.category.Category;
import com.helpdesk.common.CommonRepository;
import com.helpdesk.enums.*;
import com.helpdesk.user.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Repository
public interface TicketRepository extends CommonRepository<Ticket>,
        PagingAndSortingRepository<Ticket, Integer> {

    Optional<Category> findByCategoryId(Integer id);

    Optional<Ticket> findByIdAndOwnerId(Integer ticketId, Integer ownerId);

    Page<Ticket> findAllByOwnerId(User user, Pageable paging);

    Page<Ticket> findAllByOwnerIdAndApproverId(User user, User User, Pageable paging);

    Page<Ticket> findAllByAssigneeId(User user, Pageable paging);

    List<Ticket> findAllByAssigneeIdAndState(User user, State state);

    Page<Ticket> findAllByState(State state, Pageable paging);

    @Query(value = ("${Ticket.findByRoleAndState}"), nativeQuery = true)
    List<Ticket> findByRoleAndState(@Param("role") Role role, @Param("state") State state);

    @Query(value = ("${Ticket.findByApproverAndState}"), nativeQuery = true)
    List<Ticket> findByApproverAndState(@Param("approverId") Integer approverId, @Param("state") State state);

    Page<Ticket> findAllByIdAndOwnerId(Integer id, User authUser, Pageable paging);

    Page<Ticket> findAllByNameAndOwnerId(String name, User authUser, Pageable paging);

    Page<Ticket> findAllByDesiredResolutionDateAndOwnerId(Date desiredResolutionDate, User authUser, Pageable paging);

    Page<Ticket> findAllByUrgencyIdAndOwnerId(Urgency urgency, User authUser, Pageable paging);

    Page<Ticket> findAllByStateAndOwnerId(State state, User authUser, Pageable paging);
}

