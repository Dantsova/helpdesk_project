package com.helpdesk.ticket;

import com.helpdesk.attachment.Attachment;
import com.helpdesk.category.Category;
import com.helpdesk.comment.Comment;
import com.helpdesk.common.AbstractEntity;
import com.helpdesk.enums.State;
import com.helpdesk.enums.Urgency;
import com.helpdesk.feedback.Feedback;
import com.helpdesk.history.History;
import com.helpdesk.user.User;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

import static java.util.Objects.isNull;

@Entity(name = "Ticket")
@Table(name = "ticket", schema = "ticketdb")
public class Ticket extends AbstractEntity {

    private String name;
    private String description;
    private LocalDate createdOn;
    private LocalDate desiredResolutionDate;
    private User assigneeId;
    private User ownerId;
    private State state;
    private Category categoryId;
    private Urgency urgencyId;
    private User approverId;
//    private List<Attachment> attachmentsById;
//    private List<Comment> commentsById;
//    private List<Feedback> feedbacksById;
//    private List<History> historiesById;

    public Ticket(String name, String description, LocalDate createdOn, LocalDate desiredResolutionDate, User assigneeId, User ownerId, State state, Category categoryId, Urgency urgencyId, User approverId) {
        this.name = name;
        this.description = description;
        this.createdOn = createdOn;
        this.desiredResolutionDate = desiredResolutionDate;
        this.assigneeId = assigneeId;
        this.ownerId = ownerId;
        this.state = state;
        this.categoryId = categoryId;
        this.urgencyId = urgencyId;
        this.approverId = approverId;
    }

    public Ticket() {
    }

    @Column(name = "name", nullable = true, length = 30)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "description", nullable = true, length = 30)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Column(name = "created_on", nullable = true, length = 30)
    public LocalDate getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(LocalDate createdOn) {
        this.createdOn = createdOn;
    }

    @Column(name = "desired_resolution_date", nullable = true, length = 30)
    public LocalDate getDesiredResolutionDate() {
        return desiredResolutionDate;
    }

    public void setDesiredResolutionDate(LocalDate desiredResolutionDate) {
        this.desiredResolutionDate = desiredResolutionDate;
    }

    @ManyToOne
    @JoinColumn(name = "assignee_id", referencedColumnName = "id")
    public User getAssigneeId() {
        return assigneeId;
    }

    public void setAssigneeId(User assigneeId) {
        this.assigneeId = assigneeId;
    }

    @ManyToOne
    @JoinColumn(name = "owner_id", referencedColumnName = "id")
    public User getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(User ownerId) {
        this.ownerId = ownerId;
    }

    @Enumerated(EnumType.STRING)
    @Column(name = "state", nullable = true, length = 30)
    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    @ManyToOne
    @JoinColumn(name = "category_id", referencedColumnName = "id")
    public Category getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Category categoryId) {
        this.categoryId = categoryId;
    }

    @Enumerated(EnumType.STRING)
    @Column(name = "urgency_id")
    public Urgency getUrgencyId() {
        if (isNull(urgencyId)) {
            return urgencyId = Urgency.LOW;
        }
        return urgencyId;
    }

    public void setUrgencyId(Urgency urgencyId) {
        this.urgencyId = urgencyId;
    }

    @ManyToOne
    @JoinColumn(name="approver_id", referencedColumnName = "id")
    public User getApproverId() {
        return approverId;
    }

    public void setApproverId(User approverId) {
        this.approverId = approverId;
    }
}
