package com.helpdesk.ticket;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.helpdesk.common.AbstractDto;
import com.helpdesk.enums.State;
import com.helpdesk.enums.Urgency;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.FutureOrPresent;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PastOrPresent;
import javax.validation.constraints.Pattern;
import java.time.LocalDate;
import java.time.LocalDateTime;

import static java.util.Objects.isNull;

public class TicketDto extends AbstractDto {

    @Length(min = 1, max = 100,  message = "name: length of field should be between 1 and 100 characters")
    @Pattern(regexp="^[a-z1-9\\s~.\"(),:;<>@\\[\\]!#$%&'*+-\\\\\\/=?^_`{|}]+$", message = "Name allowed to enter lowercase English alpha characters, digits and special characters")
    private String name;

    @Length(max = 500, message = "description: length of field should be between 1 and 500 characters")
    @Pattern(regexp="^[a-z1-9\\s~.\"(),:;<>@\\[\\]!#$%&'*+-\\\\\\/=?^_`{|}]+$", message = "Allowed to enter lowercase English alpha characters, digits and special characters")
    private String description;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    @PastOrPresent(message = "createdOn: cannot be created in advance")
    private LocalDate createdOn;

    @NotNull(message = "desiredResolutionDate: is mandatory field")
    @JsonFormat (shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    @FutureOrPresent(message = "desiredResolutionDate: is less than the current date")
    private LocalDate desiredResolutionDate;

    private Integer assigneeId;

    @NotNull(message = "ownerId: mandatory field")
    private Integer ownerId;

    private State state;
    private Integer categoryId;
    private Urgency urgencyId;
    private Integer approverId;

    public TicketDto() {
    }

    public TicketDto(String name, String description, LocalDate createdOn, LocalDate desiredResolutionDate, Integer assigneeId, Integer ownerId, State state, Integer categoryId, Urgency urgencyId, Integer approverId) {
        this.name = name;
        this.description = description;
        if(isNull(createdOn)){
            this.createdOn = LocalDate.from(LocalDateTime.now ());
        }
        this.desiredResolutionDate = desiredResolutionDate;
        this.assigneeId = assigneeId;
        this.ownerId = ownerId;
        this.state = state;
        if(isNull(state)){
            this.state = State.NEW;
        }
        this.categoryId = categoryId;
        this.urgencyId = urgencyId;
        this.approverId = approverId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDate getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(LocalDate createdOn) {
        this.createdOn = createdOn;
    }

    public LocalDate getDesiredResolutionDate() {
        return desiredResolutionDate;
    }

    public void setDesiredResolutionDate(LocalDate desiredResolutionDate) {
        this.desiredResolutionDate = desiredResolutionDate;
    }

    public Integer getAssigneeId() {
        return assigneeId;
    }

    public void setAssigneeId(Integer assigneeId) {
        this.assigneeId = assigneeId;
    }

    public Integer getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(Integer ownerId) {
        this.ownerId = ownerId;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public Urgency getUrgencyId() {
        return urgencyId;
    }

    public void setUrgencyId(Urgency urgencyId) {
        this.urgencyId = urgencyId;
    }

    public Integer getApproverId() {
        return approverId;
    }

    public void setApproverId(Integer approverId) {
        this.approverId = approverId;
    }
}