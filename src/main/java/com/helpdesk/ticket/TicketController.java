package com.helpdesk.ticket;

import com.helpdesk.enums.*;
import com.helpdesk.exception.*;
import com.helpdesk.user.User;
import com.helpdesk.user.UserService;
import com.helpdesk.utils.SortBy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.security.Principal;
import java.util.Date;

import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;

@RequestMapping("/tickets")
@RestController
@Validated
public class TicketController {
    private UserService userService;
    private TicketService ticketService;
    private TicketMapper ticketMapper;

    @Autowired
    public TicketController(UserService userService, TicketService ticketService, TicketMapper ticketMapper) {
        this.userService = userService;
        this.ticketService = ticketService;
        this.ticketMapper = ticketMapper;
    }

    public TicketController() {
    }

    @PostMapping
    @PreAuthorize(value = "hasAnyAuthority('MANAGER', 'EMPLOYEE')")
    public ResponseEntity<TicketDto> create(Principal principal,
                                            @Valid @RequestBody TicketDto ticketDTO,
                                            HttpServletRequest httpRequest) throws ResourceNotFoundException, HelpDeskException {
        User authUser = userService.findByEmail(principal.getName());
        Ticket rawTicket = ticketMapper.toEntity(ticketDTO);
        Ticket createdTicket = ticketService.createWithHistoryMark(authUser, rawTicket);

        UriComponents uriComponents =
                UriComponentsBuilder
                        .newInstance()
                        .scheme(httpRequest.getScheme())
                        .host(httpRequest.getRemoteAddr())
                        .port(httpRequest.getLocalPort())
                        .path(httpRequest.getRequestURI())
                        .path("/{id}").buildAndExpand(createdTicket.getId());

        return ResponseEntity
                .status(HttpStatus.CREATED)
                .header(HttpHeaders.LOCATION, String.valueOf(uriComponents.toUri()))
                .body(ticketMapper.toDto(createdTicket));
    }

    @GetMapping("/{ticketId}")
    public ResponseEntity<TicketDto> get(Principal principal,
                                         @PathVariable(value = "ticketId", required = false) Integer ticketId) throws HelpDeskException, ResourceNotFoundException {
        User authUser = userService.findByEmail(principal.getName());
        Ticket ticket = ticketService.getByIdAndOwnerId(authUser.getId(), ticketId);
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(ticketMapper.toDto(ticket));
    }

    @GetMapping
    public ResponseEntity<Page<TicketDto>> getTickets(Principal principal,
                                                      Pageable paging,
                                                      @RequestParam(value = "ownerId", required = false) Integer ownerId,
                                                      @RequestParam(value = "state", required = false) State state) throws ResourceNotFoundException {
        Page<Ticket> ticketPage;
        User authUser = userService.findByEmail(principal.getName());
        User owner = userService.get(ownerId);
        if (nonNull(ownerId) && isNull(state)) {
            ticketPage = ticketService.getByOwner(owner, paging);
        }
        if (isNull(ownerId) && nonNull(state)) {
            ticketPage = ticketService.getByState(state, paging);
        }
        if (nonNull(ownerId) && nonNull(state)) {
            ticketPage = ticketService.getByStateAndOwner(state, owner, paging);
        }
        if (isNull(ownerId) && isNull(state)) {
            ticketPage = ticketService.getFilteredTicketByRole(authUser, paging);
        }
        ticketPage = SortBy.getByUrgencyAndCreatedOn(ticketService.getByRole(authUser, paging));
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(ticketPage.map(ticketMapper::toDto));
    }

    @GetMapping("/sort")
    public ResponseEntity<Page<TicketDto>> getByOrder(
            Principal principal,
            Pageable pageable,
            @RequestParam(name = "id", required = false) Integer id,
            @RequestParam(name = "name", required = false) String name,
            @RequestParam(name = "desiredResolutionDate", required = false) Date desiredResolutionDate,
            @RequestParam(name = "urgency", required = false) Urgency urgency,
            @RequestParam(name = "state", required = false) State state
    ) throws ResourceNotFoundException {
        User authUser = userService.findByEmail(principal.getName());
        Page<Ticket> ticketPage;
        if (isNull(id) & isNull(name) & isNull(desiredResolutionDate) & isNull(urgency) & isNull(state)) {
            ticketPage = ticketService.getBySort(authUser, pageable);
        } else {
            ticketPage = ticketService.getByFilter(authUser, id, name, desiredResolutionDate, urgency, state, pageable);
        }
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(ticketPage.map(ticketMapper::toDto));
    }

    @PutMapping("/{id}/actions/{action}")
    @PreAuthorize(value = "hasAnyAuthority('MANAGER', 'EMPLOYEE')")
    ResponseEntity<TicketDto> changeState(Principal principal,
                                          @PathVariable(value = "id") Integer ticketId,
                                          @PathVariable(value = "action") Action action) throws ResourceNotFoundException, HelpDeskException {
        User authUser = userService.findByEmail(principal.getName());
        Ticket ticket = ticketService.transmitState(authUser, ticketId, action);
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(ticketMapper.toDto(ticket));
    }

    @PatchMapping("/{id}")
    @PreAuthorize(value = "hasAnyAuthority('MANAGER', 'EMPLOYEE')")
    public ResponseEntity<TicketDto> update(Principal principal,
                                            @RequestBody TicketDto rawDtoTicket,
                                            @PathVariable(value = "id") Integer ticketId) throws ResourceNotFoundException, HelpDeskException {
        User authUser = userService.findByEmail(principal.getName());
        Ticket ticketToUpdate = ticketService.get(ticketId);
        ticketMapper.updateFromDtoToEntity(rawDtoTicket, ticketToUpdate);
        ticketService.addHistoryOnUpdate(authUser, ticketId, ticketToUpdate);
        Ticket ticket = ticketService.save(ticketToUpdate);
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(ticketMapper.toDto(ticket));
    }

    @DeleteMapping
    @PreAuthorize(value = "hasAnyAuthority('MANAGER', 'EMPLOYEE')")
    public ResponseEntity<TicketDto> delete(@PathVariable(value = "id") Integer id) throws ResourceNotFoundException {
        boolean isExists = ticketService.existsById(id);
        if (!isExists) {
            return ResponseEntity
                    .status(HttpStatus.NOT_FOUND)
                    .build();
        }
        ticketService.delete(id);
        return ResponseEntity
                .status(HttpStatus.NO_CONTENT)
                .build();
    }
}