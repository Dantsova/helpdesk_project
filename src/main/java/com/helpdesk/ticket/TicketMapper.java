package com.helpdesk.ticket;

import com.helpdesk.category.Category;
import com.helpdesk.category.CategoryService;
import com.helpdesk.common.CommonMapper;
import com.helpdesk.exception.ResourceNotFoundException;
import com.helpdesk.user.User;
import com.helpdesk.user.UserService;
import org.mapstruct.*;
import org.springframework.beans.factory.annotation.Autowired;


@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR)
public abstract class TicketMapper implements CommonMapper<Ticket, TicketDto> {
    @Autowired
    private UserService userService;

    @Autowired
    private CategoryService categoryService;

    @Mapping(target = "ownerId", source = "ownerId.id")
    @Mapping(target = "assigneeId", source = "assigneeId.id")
    @Mapping(target = "approverId", source = "approverId.id")
    @Mapping(target = "categoryId", source = "categoryId.id")
    public abstract TicketDto toDto(Ticket ticket);

    @Mapping(target = "ownerId", source = "ownerId", qualifiedByName = "mapUser")
    @Mapping(target = "assigneeId", source = "assigneeId", qualifiedByName = "mapUser")
    @Mapping(target = "approverId", source = "approverId", qualifiedByName = "mapUser")
    @Mapping(target = "categoryId", source = "categoryId", qualifiedByName = "mapCategory")
    public abstract Ticket toEntity(TicketDto ticketDTO);

    @Mapping(target = "id", ignore = true)
    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    @Mapping(target = "assigneeId", source = "assigneeId", qualifiedByName = "mapUser")
    @Mapping(target = "ownerId", source = "ownerId", qualifiedByName = "mapUser")
    @Mapping(target = "approverId", source = "approverId", qualifiedByName = "mapUser")
    @Mapping(target = "categoryId", source = "categoryId", qualifiedByName = "mapCategory")
    public abstract void updateFromDtoToEntity(TicketDto ticketDto, @MappingTarget Ticket ticket);

    @Named("mapUser")
    protected User mapUser(Integer userId) throws ResourceNotFoundException {
        return userService.get(userId);
    }

    @Named("mapCategory")
    protected Category mapCategory(Integer categoryId) throws ResourceNotFoundException {
        return categoryService.get(categoryId);
    }
}