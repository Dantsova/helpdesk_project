package com.helpdesk.feedback;

import com.helpdesk.common.AbstractService;
import com.helpdesk.enums.State;
import com.helpdesk.exception.HelpDeskException;
import com.helpdesk.exception.ResourceNotFoundException;
import com.helpdesk.ticket.Ticket;
import com.helpdesk.ticket.TicketService;
import com.helpdesk.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class FeedbackService extends AbstractService<Feedback, FeedbackRepository> {
    private final TicketService ticketService;

    @Autowired
    public FeedbackService(FeedbackRepository repository, TicketService ticketService) {
        super(repository);
        this.ticketService = ticketService;
    }

    public Feedback create(User authUser, Feedback rawFeedback) throws HelpDeskException, ResourceNotFoundException {
        if (authUser.getId()!=(rawFeedback.getUserId().getId())){
            throw new HelpDeskException( authUser.getEmail() + " have not rights to create a feedback. Providing feedback is available only for the User who created the ticket.");
        }
        Ticket ticket = ticketService.get(rawFeedback.getTicketId().getId());
        if(!ticket.getState().equals(State.DONE)){
            throw new HelpDeskException("Providing feedback is available only for tickets in Done");
        }
        return repository.save(rawFeedback);
    }

    public Page<Feedback> getAllByTicketId(Integer ticketId, Pageable paging) {
        return repository.findAllByTicketId(ticketId, paging);
    }

    public Feedback getByTicketId(Ticket ticket, Integer feedbackId) throws ResourceNotFoundException {
        return repository.findByIdAndTicketId(ticket, feedbackId)
                .orElseThrow(()->new ResourceNotFoundException(feedbackId+"in ticketId"+ticket.getId()));
    }
}
