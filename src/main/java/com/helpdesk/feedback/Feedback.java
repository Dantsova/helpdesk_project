package com.helpdesk.feedback;

import com.helpdesk.common.AbstractEntity;
import com.helpdesk.ticket.Ticket;
import com.helpdesk.user.User;

import javax.persistence.*;
import java.sql.Date;

@Entity
@Table(name = "FEEDBACK", schema = "ticketdb")
public class Feedback extends AbstractEntity {
    private User userId;
    private Integer rate;
    private Date date;
    private String text;
    private Ticket ticketId;

    public Feedback(User userId, Integer rate, Date date, String text, Ticket ticketId) {
        this.userId = userId;
        this.rate = rate;
        this.date = date;
        this.text = text;
        this.ticketId = ticketId;
    }

    public Feedback() {
    }

    @ManyToOne
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    public User getUserId() {
        return userId;
    }

    public void setUserId(User userId) {
        this.userId = userId;
    }

    @Column(name = "rate", nullable = true)
    public Integer getRate() {
        return rate;
    }

    public void setRate(Integer rate) {
        this.rate = rate;
    }

    @Column(name = "date", nullable = true)
    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Column(name = "text", nullable = true, length = 30)
    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @ManyToOne
    @JoinColumn(name = "ticket_id", referencedColumnName = "id")
    public Ticket getTicketId() {
        return ticketId;
    }

    public void setTicketId(Ticket ticketId) {
        this.ticketId = ticketId;
    }
}