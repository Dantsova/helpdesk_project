package com.helpdesk.feedback;

import com.helpdesk.common.CommonMapper;
import com.helpdesk.exception.ResourceNotFoundException;
import com.helpdesk.ticket.Ticket;
import com.helpdesk.ticket.TicketService;
import com.helpdesk.user.User;
import com.helpdesk.user.UserService;
import org.mapstruct.*;
import org.springframework.beans.factory.annotation.Autowired;

@Mapper(componentModel = "spring",injectionStrategy = InjectionStrategy.CONSTRUCTOR)
public abstract class FeedbackMapper implements CommonMapper<Feedback, FeedbackDto> {
    @Autowired
    private UserService userService;

    @Autowired
    private TicketService ticketService;

    @Mapping(target = "userId", source = "userId.id")
    @Mapping(target = "ticketId", source = "ticketId.id")
    public abstract FeedbackDto toDto (Feedback feedbackEntity);

    @Mapping(target = "userId", source = "userId", qualifiedByName = "mapUser")
    @Mapping(target = "ticketId", source = "ticketId",qualifiedByName = "mapTicket")
    public abstract Feedback toEntity(FeedbackDto feedbackDto);

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "userId", source = "userId", qualifiedByName = "mapUser")
    @Mapping(target = "ticketId", source = "ticketId",qualifiedByName = "mapTicket")
    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    public abstract void updateFromDtoToEntity(FeedbackDto feedbackDto, @MappingTarget Feedback feedback);

    @Named("mapUser")
    protected User mapUser(Integer userId) throws ResourceNotFoundException {
        return userService.get(userId);
    }

    @Named("mapTicket")
    protected Ticket mapTicket(Integer ticketId) throws ResourceNotFoundException {
        return ticketService.get(ticketId);
    }
}
