package com.helpdesk.feedback;


import com.helpdesk.common.CommonRepository;
import com.helpdesk.ticket.Ticket;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface FeedbackRepository extends CommonRepository<Feedback>,
        PagingAndSortingRepository<Feedback, Integer> {

    Page<Feedback> findAllByTicketId(Integer ticketId, Pageable paging);

    Optional<Feedback> findByIdAndTicketId(Ticket ticket, Integer feedbackId);
}
