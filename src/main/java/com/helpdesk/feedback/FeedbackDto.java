package com.helpdesk.feedback;

import com.helpdesk.common.AbstractDto;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.sql.Date;

public class FeedbackDto extends AbstractDto {
    private Integer userId;

    @NotNull
    @Min(value = 1 , message = "scale from 1 to 5")
    @Max(5)
    private Integer rate;

    private Date date;
    private String text;
    private Integer ticketId;

    public FeedbackDto(Integer userId, Integer rate, Date date, String text, Integer ticketId) {
        this.userId = userId;
        this.rate = rate;
        this.date = date;
        this.text = text;
        this.ticketId = ticketId;
    }

    public FeedbackDto() {
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getRate() {
        return rate;
    }

    public void setRate(Integer rate) {
        this.rate = rate;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Integer getTicketId() {
        return ticketId;
    }

    public void setTicketId(Integer ticketId) {
        this.ticketId = ticketId;
    }
}
