package com.helpdesk.feedback;

import com.helpdesk.exception.HelpDeskException;
import com.helpdesk.exception.ResourceNotFoundException;
import com.helpdesk.ticket.Ticket;
import com.helpdesk.ticket.TicketService;
import com.helpdesk.user.User;
import com.helpdesk.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.security.Principal;

@RequestMapping("/tickets")
@RestController
public class FeedbackController {
    private final FeedbackService feedbackService;
    private final FeedbackMapper feedbackMapper;

    @Autowired
    UserService userService;

    @Autowired
    TicketService ticketService;

    @Autowired
    public FeedbackController(FeedbackService feedbackService, FeedbackMapper feedbackMapper) {
        this.feedbackService = feedbackService;
        this.feedbackMapper = feedbackMapper;
    }

    @PostMapping("/{ticketId}/feedbacks")
    public ResponseEntity<FeedbackDto> create(Principal principal,
                                              @PathVariable(value = "ticketId") Integer ticketId,
                                              @Valid @RequestBody FeedbackDto entityDTO,
                                              HttpServletRequest httpRequest) throws ResourceNotFoundException, HelpDeskException {

        User authUser = userService.findByEmail(principal.getName());
        entityDTO.setTicketId(ticketId);
        Feedback rawFeedback = feedbackMapper.toEntity(entityDTO);
        Feedback newEntity = feedbackService.create(authUser, rawFeedback);

        UriComponents uriComponents =
                UriComponentsBuilder
                        .newInstance()
                        .scheme(httpRequest.getScheme())
                        .host(httpRequest.getRemoteAddr())
                        .port(httpRequest.getLocalPort())
                        .path(httpRequest.getRequestURI())
                        .path("/{id}").buildAndExpand(newEntity.getId());

        return ResponseEntity
                .status(HttpStatus.CREATED)
                .header(HttpHeaders.LOCATION, String.valueOf(uriComponents.toUri()))
                .body(feedbackMapper.toDto(newEntity));
    }

    @GetMapping("/{ticketId}/feedbacks")
    public ResponseEntity<Page<FeedbackDto>> getAll(Pageable paging,
                                                    @PathVariable(value = "ticketId") Integer ticketId) {
        Page<Feedback> feedbackPage = feedbackService.getAllByTicketId(ticketId, paging);
        return ResponseEntity
                .ok(feedbackPage.map(feedbackMapper::toDto));
    }

    @GetMapping("/{ticketId}/feedbacks/{feedbackId}")
    public ResponseEntity<FeedbackDto> get(@PathVariable(value = "ticketId") Integer ticketId,
                                           @PathVariable(value = "feedbackId") Integer feedbackId) throws ResourceNotFoundException {
        Ticket ticket = ticketService.get(ticketId);
        Feedback feedback = feedbackService.getByTicketId(ticket, feedbackId);
        return ResponseEntity.ok(feedbackMapper.toDto(feedback));
    }

    @PatchMapping("/{ticketId}/feedbacks/{feedbackId}")
    public ResponseEntity<FeedbackDto> update(@RequestBody FeedbackDto rawFeedbackDto,
                                              @PathVariable(value = "feedbackId") Integer feedbackId) throws ResourceNotFoundException {
        Feedback feedbackToUpdate = feedbackService.get(feedbackId);
        feedbackMapper.updateFromDtoToEntity(rawFeedbackDto, feedbackToUpdate);
        return ResponseEntity
                .ok(feedbackMapper.toDto(feedbackService.save(feedbackToUpdate)));
    }

    @DeleteMapping("/{ticketId}/feedbacks/{feedbackId}")
    public ResponseEntity<FeedbackDto> delete(@PathVariable(value = "ticketId") Integer ticketId,
                                              @PathVariable(value = "feedbackId") Integer feedbackId) throws ResourceNotFoundException {
        boolean isExists = feedbackService.existsById(feedbackId);
        if (!isExists) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        feedbackService.delete(feedbackId);
        return ResponseEntity
                .noContent()
                .build();
    }
}
