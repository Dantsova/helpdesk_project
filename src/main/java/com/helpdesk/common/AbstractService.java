package com.helpdesk.common;

import com.helpdesk.exception.HelpDeskException;
import com.helpdesk.exception.ResourceNotFoundException;
import com.helpdesk.user.User;
import org.springframework.data.domain.Page;

public class AbstractService <E extends AbstractEntity, R extends CommonRepository<E>>
        implements CommonService<E>{
    protected final R repository;

    public AbstractService(R repository) {
        this.repository = repository;
    }

    @Override
    public E create(User authUser, E e) throws HelpDeskException, ResourceNotFoundException {
        return repository.save(e);
    }

    @Override
    public E get(Integer id) throws ResourceNotFoundException {
        return repository
                .findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(id + ""));
    }

    @Override
    public void delete(Integer id) throws ResourceNotFoundException {
        E entityToDelete = get(id);
        repository.delete(entityToDelete);
    }

    @Override
    public E save(E e) {
        return repository.save(e);
    }

    @Override
    public boolean existsById(Integer id) {
        return repository.existsById(id);
    }

    public Page<E> getAll() {
        return (Page<E>)repository.findAll();
    }
}