package com.helpdesk.common;

import com.helpdesk.exception.HelpDeskException;
import com.helpdesk.exception.ResourceNotFoundException;
import com.helpdesk.user.User;
import org.springframework.data.domain.Page;

public interface CommonService<E extends AbstractEntity> {

    E create(User authUser, E e) throws HelpDeskException, ResourceNotFoundException;

    E get(Integer id) throws ResourceNotFoundException;

    void delete(Integer id) throws ResourceNotFoundException;

    E save(E e);

    boolean existsById(Integer id);
}
