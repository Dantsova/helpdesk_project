package com.helpdesk.common;

import java.util.List;

public interface CommonMapper<E extends  AbstractEntity, D extends AbstractDto> {
    D toDto(E entity);

    E toEntity(D entityDTO);

    List<D> toDtoList(List<E> entityList);
}
