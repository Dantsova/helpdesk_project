package com.helpdesk.category;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/categories")
public class CategoryController  {
    private final CategoryService categoryService;
    private final CategoryMapper categoryMapper;

    @Autowired
    public CategoryController(CategoryService categoryService, CategoryMapper categoryMapper) {
        this.categoryService = categoryService;
        this.categoryMapper = categoryMapper;
    }

    @GetMapping("/{ticketId}")
    public ResponseEntity<Page<CategoryDto>> getAll(Pageable paging,
                                                    @PathVariable(value = "ticketId") Integer ticketId) {
        Page<Category> categoryPage = categoryService.getAllByTicketId(ticketId, paging);
        return ResponseEntity
                .ok(categoryPage.map(categoryMapper::toDto));
    }
}