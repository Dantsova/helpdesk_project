package com.helpdesk.category;

import com.helpdesk.common.AbstractDto;
import com.helpdesk.ticket.Ticket;

import java.util.List;

public class CategoryDto extends AbstractDto {

    private String name;
    private List<Ticket> tickets;

    public CategoryDto() {
    }

    public CategoryDto(String name, List<Ticket> tickets) {
        this.name = name;
        this.tickets = tickets;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Ticket> getTickets() {
        return tickets;
    }

    public void setTickets(List<Ticket> tickets) {
        this.tickets = tickets;
    }
}
