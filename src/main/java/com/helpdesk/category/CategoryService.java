package com.helpdesk.category;


import com.helpdesk.common.AbstractService;
import com.helpdesk.feedback.Feedback;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class CategoryService extends AbstractService<Category, CategoryRepository> {

    @Autowired
    public CategoryService(CategoryRepository repository) {
        super(repository);
    }

    public Page<Category> getAllByTicketId(Integer ticketId, Pageable paging) {
        return repository.findAllByTicketId(ticketId, paging);
    }
}