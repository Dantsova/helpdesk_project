package com.helpdesk.category;

import com.helpdesk.common.CommonMapper;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring",injectionStrategy = InjectionStrategy.CONSTRUCTOR)
public interface CategoryMapper extends CommonMapper<Category, CategoryDto> {
}
