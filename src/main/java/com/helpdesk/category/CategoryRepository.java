package com.helpdesk.category;

import com.helpdesk.common.CommonRepository;
import com.helpdesk.feedback.Feedback;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

@Repository
public interface CategoryRepository extends CommonRepository<Category> {
    Page<Category> findAllByTicketId(Integer ticketId, Pageable paging);
}