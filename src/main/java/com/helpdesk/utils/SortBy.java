package com.helpdesk.utils;

import com.helpdesk.ticket.Ticket;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;

import java.util.Comparator;

public class SortBy {
    public static Page<Ticket> getByUrgencyAndCreatedOn(Page<Ticket> ticketPage) {
            ticketPage.getContent().sort(Comparator.comparing(Ticket::getUrgencyId)
                    .thenComparing(Ticket::getCreatedOn));
    return ticketPage;
    }
}