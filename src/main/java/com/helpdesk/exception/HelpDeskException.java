package com.helpdesk.exception;

public class HelpDeskException extends Exception{
    public HelpDeskException(String message) {
        super(message);
    }
}