package com.helpdesk.exception;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.web.context.request.WebRequest;

import java.time.LocalDateTime;

public class ErrorResponseDto {
    @JsonProperty("timestamp")
    private LocalDateTime timestamp;

    @JsonProperty("status")
    private int httpStatus;

    @JsonProperty("error")
    private String error;

    @JsonProperty("message")
    private String message;

    @JsonProperty("path")
    private String path;

    public ErrorResponseDto(LocalDateTime timestamp, int httpStatus, String error, String message, WebRequest path) {
        this.timestamp = timestamp;
        this.httpStatus = httpStatus;
        this.error = error;
        this.message = message;
        this.path = path.getContextPath();
    }

    public ErrorResponseDto() {
    }
}
