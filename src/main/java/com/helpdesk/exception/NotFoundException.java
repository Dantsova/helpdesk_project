package com.helpdesk.exception;

public class NotFoundException extends RuntimeException {
    private String object;

    public NotFoundException(String object) {
        this.object = object;
    }

    @Override
    public String getMessage() {
        return  String.format("%s is not found", object);
    }
}
