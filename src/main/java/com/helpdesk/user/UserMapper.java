package com.helpdesk.user;

import com.helpdesk.common.CommonMapper;
import org.mapstruct.*;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface UserMapper extends CommonMapper<User,UserDto> {

    @Mapping(target = "id", ignore = true)
    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    void updateFromDtoToEntity(UserDto userDto, @MappingTarget User user);
}