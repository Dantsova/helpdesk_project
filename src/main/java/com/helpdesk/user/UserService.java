package com.helpdesk.user;

import com.helpdesk.common.AbstractService;
import com.helpdesk.exception.ResourceNotFoundException;
import com.helpdesk.enums.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class UserService extends AbstractService<User, UserRepository> {

    @Autowired
    public UserService(UserRepository userRepository) {
        super(userRepository);
    }

    public Page<User> getAllUsers(Pageable pageable) {
        return repository.findAll(pageable);
    }

    public Page<User> getByRole(Pageable pageable, Role role) throws ResourceNotFoundException {
        if (repository.findByRole(pageable, role).isEmpty()) {
            throw new ResourceNotFoundException("Role");
        }
        return repository.findByRole(pageable, role);
    }

    public Page<User> getByFirstName(Pageable pageable, String firstName) throws ResourceNotFoundException {
        if (repository.findByFirstName(pageable, firstName).isEmpty()) {
            throw new ResourceNotFoundException("FirstName");
        }
        return repository.findByFirstName(pageable, firstName);
    }

    public User findByEmail(String email) throws ResourceNotFoundException {
        return repository.findByEmail(email)
                .orElseThrow(() -> new ResourceNotFoundException("Email"));
    }

    public User get(Integer id) throws ResourceNotFoundException {
        return repository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("User"));
    }
}