package com.helpdesk.user;

import com.helpdesk.common.CommonRepository;
import com.helpdesk.enums.Role;
import com.helpdesk.ticket.Ticket;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import java.util.Optional;

@Repository
public interface UserRepository extends CommonRepository<User>,
        PagingAndSortingRepository<User, Integer> {

    Page<User> findByFirstName(Pageable pageable, String firstName);

    Page<User> findByRole(Pageable pageable, Role role);

    Optional<User> findByEmail(String email);
}