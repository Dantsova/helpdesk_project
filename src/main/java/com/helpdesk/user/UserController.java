package com.helpdesk.user;

import com.helpdesk.exception.HelpDeskException;
import com.helpdesk.exception.ResourceNotFoundException;
import com.helpdesk.enums.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.security.Principal;

import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;

@RestController
@RequestMapping("/users")
public class UserController {
    private final UserService userService;
    private final UserMapper userMapper;

    @Autowired
    public UserController(UserService userService, UserMapper userMapper) {
        this.userService = userService;
        this.userMapper = userMapper;
    }

    @PostMapping
    public ResponseEntity<UserDto> create(Principal principal,
                                          @Valid @RequestBody UserDto userDto,
                                          HttpServletRequest httpRequest) throws ResourceNotFoundException, HelpDeskException {
        User authUser = userService.findByEmail(principal.getName());
        User rawEntity = userMapper.toEntity(userDto);
        User newEntity = userService.create(authUser, rawEntity);

        UriComponents uriComponents =
                UriComponentsBuilder
                        .newInstance()
                        .scheme(httpRequest.getScheme())
                        .host(httpRequest.getRemoteAddr())
                        .port(httpRequest.getLocalPort())
                        .path(httpRequest.getRequestURI())
                        .path("/{id}").buildAndExpand(newEntity.getId());
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .header(HttpHeaders.LOCATION, String.valueOf(uriComponents.toUri()))
                .body(userMapper.toDto(newEntity));
    }

    @GetMapping
    public ResponseEntity<Page<UserDto>> read(Pageable pageable,
                                              @RequestParam(value = "role", required = false) Role userRole,
                                              @RequestParam(value = "firstName", required = false) String userFirstName) throws ResourceNotFoundException {
        Page<User> userPage = null;
        if (nonNull(userRole)) {
            userPage = userService.getByRole(pageable, userRole);
        }
        if (nonNull(userFirstName)) {
            userPage = userService.getByFirstName(pageable, userFirstName);
        }
        if (isNull(userRole) && isNull(userFirstName)) {
            userPage = userService.getAllUsers(pageable);
        }
        userPage.map(userMapper::toDto);
        return new ResponseEntity(userPage, HttpStatus.OK);
    }

    @GetMapping("/{userId}")
    public ResponseEntity<UserDto> getById(
            @PathVariable(value = "userId") Integer userId) throws ResourceNotFoundException {
        User user = userService.get(userId);
        return ResponseEntity
                .ok(userMapper.toDto(user));
    }

    @PatchMapping ("/{id}")
    public ResponseEntity<UserDto> update(@RequestBody UserDto rawUser,
                                   @PathVariable(value = "id") Integer id) throws ResourceNotFoundException {
        User userToUpdate = userService.get(id);
        userMapper.updateFromDtoToEntity(rawUser,userToUpdate);
        return ResponseEntity
                .ok(userMapper.toDto(userService.save(userToUpdate)));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<UserDto> delete(@PathVariable(value = "id") Integer id) throws ResourceNotFoundException {
        boolean isExists = userService.existsById(id);
        if (!isExists) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        userService.delete(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}