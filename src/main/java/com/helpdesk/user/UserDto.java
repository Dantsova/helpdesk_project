package com.helpdesk.user;

import com.helpdesk.common.AbstractDto;
import com.helpdesk.enums.Role;

import javax.validation.constraints.*;

public class UserDto extends AbstractDto {
    private String firstName;
    private String lastName;
    private Role role;

    @Email(message = "Email should be valid")
    @NotEmpty(message = "Please fill out the required field Email.")
    private String email;

    @NotBlank(message = "Please fill out the required field Password")
    @Pattern(regexp = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[~.\"(),:;<>@\\[\\]!#$%&'*+-\\\\\\/=?^_`{|}]).{6,20}$",
            message = "pass should contain at least one uppercase and one lowercase English alpha characters, one digit and one special character and required input 6-20 characters")
    private String password;

    public UserDto() {
    }

    public UserDto(String firstName, String lastName, Role role, String email, String password) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.role = role;
        this.email = email;
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}