package com.helpdesk.attachment;


import com.helpdesk.common.CommonRepository;
import com.helpdesk.feedback.Feedback;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AttachmentRepository extends CommonRepository<Attachment>,
        PagingAndSortingRepository<Attachment, Integer> {

    Page<Attachment> findAllByTicketId(Integer ticketId, Pageable paging);
}
