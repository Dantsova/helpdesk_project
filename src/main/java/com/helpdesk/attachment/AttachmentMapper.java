package com.helpdesk.attachment;

import com.helpdesk.common.CommonMapper;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR)
public interface AttachmentMapper extends CommonMapper<Attachment, AttachmentDto> {

    @Override
    @Mapping(target = "ticketId", source = "ticketId.id")
    AttachmentDto toDto(Attachment attachment);

    @Override
    @Mapping(target = "ticketId.id",source = "ticketId")
    Attachment toEntity(AttachmentDto attachmentDto);
}
