package com.helpdesk.attachment;

import com.helpdesk.common.AbstractDto;

import javax.validation.constraints.Pattern;

public class AttachmentDto extends AbstractDto {

    private String url;
    private Integer ticketId;

    @Pattern(regexp = "^[a-z1-9]+.[pdf|jpe?g|png|docx?]{3,4}$")
    private String name;

    public AttachmentDto(String url, Integer ticketId, String name) {
        this.url = url;
        this.ticketId = ticketId;
        this.name = name;
    }

    public AttachmentDto() {
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Integer getTicketId() {
        return ticketId;
    }

    public void setTicketId(Integer ticketId) {
        this.ticketId = ticketId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}