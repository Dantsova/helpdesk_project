package com.helpdesk.attachment;

import com.helpdesk.exception.*;
import com.helpdesk.user.User;
import com.helpdesk.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.security.Principal;

@RequestMapping("/attachments")
@RestController
public class AttachmentController {
    private final UserService userService;
    private final AttachmentService attachmentService;
    private final AttachmentMapper attachmentMapper;

    @Autowired
    public AttachmentController(UserService userService, AttachmentService attachmentService, AttachmentMapper attachmentMapper) {
        this.userService = userService;
        this.attachmentService = attachmentService;
        this.attachmentMapper = attachmentMapper;
    }

    @PostMapping
    @PreAuthorize(value = "hasAnyAuthority('MANAGER', 'EMPLOYEE')")
    public ResponseEntity<AttachmentDto> create(Principal principal,
                                                @Valid @RequestBody AttachmentDto rawAttachment,
                                                HttpServletRequest httpRequest) throws ResourceNotFoundException, HelpDeskException {
        User authUser = userService.findByEmail(principal.getName());
        Attachment attachment = attachmentMapper.toEntity(rawAttachment);
        Attachment newAttachment = attachmentService.create(authUser, attachment);
        UriComponents uriComponents =
                UriComponentsBuilder
                        .newInstance()
                        .scheme(httpRequest.getScheme())
                        .host(httpRequest.getRemoteAddr())
                        .port(httpRequest.getLocalPort())
                        .path(httpRequest.getRequestURI())
                        .path("/{id}").buildAndExpand(newAttachment.getId());
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .header(HttpHeaders.LOCATION, String.valueOf(uriComponents.toUri()))
                .body(attachmentMapper.toDto(newAttachment));
    }

    @GetMapping("/tickets/{attachmentId}")
    public ResponseEntity<Page<AttachmentDto>> getAttachmentsByTicketId(Pageable paging,
                                                                        @PathVariable("attachmentId") Integer attachmentId) {
        Page<Attachment> attachment = attachmentService.getAttachmentsByTicketId(attachmentId, paging);
        return ResponseEntity
                .ok(attachment.map(attachmentMapper::toDto));
    }

    @DeleteMapping("/{ticketId}/attachments/{attachmentId}")
    public ResponseEntity<AttachmentDto> delete(@PathVariable(value = "attachmentId") Integer attachmentId) throws ResourceNotFoundException {
        boolean isExists = attachmentService.existsById(attachmentId);
        if (!isExists) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        attachmentService.delete(attachmentId);
        return ResponseEntity
                .noContent()
                .build();
    }
}