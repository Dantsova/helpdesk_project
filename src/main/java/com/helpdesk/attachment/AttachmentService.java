package com.helpdesk.attachment;


import com.helpdesk.common.AbstractService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class AttachmentService extends AbstractService<Attachment, AttachmentRepository> {

    private AttachmentRepository attachmentRepository;

    @Autowired
    public AttachmentService(AttachmentRepository attachmentRepository) {
        super(attachmentRepository);
    }

    public Page<Attachment> getAttachmentsByTicketId(Integer ticketId, Pageable paging) {
       return attachmentRepository.findAllByTicketId(ticketId, paging);
    }
}
