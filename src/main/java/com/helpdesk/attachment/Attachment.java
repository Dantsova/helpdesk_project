package com.helpdesk.attachment;

import com.helpdesk.common.AbstractEntity;
import com.helpdesk.ticket.Ticket;

import javax.persistence.*;
import java.util.Arrays;

@Entity
@Table(name = "ATTACHMENT", schema = "ticketdb")
public class Attachment extends AbstractEntity {
    private String url;
    private Ticket ticketId;
    private String name;

    @Column(name = "url")
    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @ManyToOne
    @JoinColumn(name = "ticket_id", referencedColumnName = "id")
    public Ticket getTicketId() {
        return ticketId;
    }

    public void setTicketId(Ticket ticketId) {
        this.ticketId = ticketId;
    }

    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}