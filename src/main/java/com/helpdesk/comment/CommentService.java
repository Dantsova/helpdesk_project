package com.helpdesk.comment;

import com.helpdesk.common.AbstractService;
import org.springframework.stereotype.Service;

@Service
public class CommentService extends AbstractService<Comment, CommentRepository> {

    public CommentService(CommentRepository repository) {
        super(repository);
    }
}
