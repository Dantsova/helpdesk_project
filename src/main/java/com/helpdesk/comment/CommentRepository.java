package com.helpdesk.comment;

import com.helpdesk.common.CommonRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CommentRepository extends CommonRepository<Comment> {
}
