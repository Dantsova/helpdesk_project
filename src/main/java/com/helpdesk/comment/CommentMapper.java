package com.helpdesk.comment;

import com.helpdesk.common.CommonMapper;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring",injectionStrategy = InjectionStrategy.CONSTRUCTOR)
public interface CommentMapper extends CommonMapper<Comment, CommentDto> {

    @Mapping(target = "ticket.id", source = "id")
    @Mapping(target = "user.id", source = "id")
    CommentDto toDto(Comment commentEntity);
}
