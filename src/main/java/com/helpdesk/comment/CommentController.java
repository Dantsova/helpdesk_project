package com.helpdesk.comment;

import org.springframework.beans.factory.annotation.Autowired;

public class CommentController {
    private final CommentService commentService;
    private final CommentMapper commentMapper;

    @Autowired
    public CommentController(CommentService commentService, CommentMapper commentMapper) {
         this.commentService = commentService;
        this.commentMapper = commentMapper;
    }
}
