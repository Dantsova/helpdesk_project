package com.helpdesk.comment;

import com.helpdesk.common.AbstractEntity;
import com.helpdesk.ticket.Ticket;
import com.helpdesk.user.User;

import javax.persistence.*;
import java.sql.Date;

@Entity
@Table(name = "COMMENT", schema = "ticketdb")
public class Comment extends AbstractEntity {
    private Integer userId;
    private String text;
    private Date date;
    private Integer ticketId;
    private User userByUserId;
    private Ticket ticketByTicketId;

    @Column(name = "user_id", nullable = true, insertable = false, updatable = false)
    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    @Column(name = "text", nullable = true, length = 30)
    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Column(name = "date", nullable = true)
    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Column(name = "ticket_id", nullable = true, insertable = false, updatable = false)
    public Integer getTicketId() {
        return ticketId;
    }

    public void setTicketId(Integer ticketId) {
        this.ticketId = ticketId;
    }

    @ManyToOne
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    public User getUserByUserId() {
        return userByUserId;
    }

    public void setUserByUserId(User userByUserId) {
        this.userByUserId = userByUserId;
    }

    @ManyToOne
    @JoinColumn(name = "ticket_id", referencedColumnName = "id")
    public Ticket getTicketByTicketId() {
        return ticketByTicketId;
    }

    public void setTicketByTicketId(Ticket ticketByTicketId) {
        this.ticketByTicketId = ticketByTicketId;
    }
}