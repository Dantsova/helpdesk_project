package com.helpdesk.comment;

import com.helpdesk.common.AbstractDto;
import com.helpdesk.ticket.Ticket;
import com.helpdesk.user.User;

import javax.validation.constraints.Max;
import javax.validation.constraints.Pattern;
import java.util.Date;

public class CommentDto extends AbstractDto {
    private User user;

    @Max(value = 500 , message = "Maximum number of characters is 500")
    @Pattern(regexp = "[a-zA-Z0-9~.(),:;<>@!#$%&'*+-=?^_`{|}]", message = "Text allowed to enter upper and lowercase English alpha characters, digits and special characters")
    private String text;

    private Date date;
    private Ticket ticket;

    public CommentDto() {
    }

    public CommentDto(User user, String text, Date date, Ticket ticket) {
        this.user = user;
        this.text = text;
        this.date = date;
        this.ticket = ticket;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

   public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Ticket getTicket() {
        return ticket;
    }

    public void setTicket(Ticket ticket) {
        this.ticket = ticket;
    }
}